// Set up the dependencies
// Require is used to relate other files to our current file
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
// Import routes
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')

// Server setup
const app = express(); 
const port = 3000;

app.use(cors());
// Tells server that cors is being used by your server

app.use(express.json());
app.use(express.urlencoded({extended:true}))

app.use("/users", userRoutes)
app.use("/products", productRoutes)
app.use("/orders", orderRoutes)

mongoose.connect("mongodb+srv://admin:admin123@cluster0.7csi8.mongodb.net/capstone-2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})

mongoose.connection.once('open', () => {
	console.log("Now connected to MongoDB Atlas")
})

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
})