const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	sku: {
		type: String,
		required: [true, "SKU is required"]
	},
	productName: {
		type: String,
		required: [true, "Product Name is required"]
	},
	brand:{
		type: String,
		required: [true, "Brand is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	qty: {
		type: Number,
		default: 0
	},
	dimensions: [
		{
			weight: {
				type: Number,
			},
			length: {
				type: Number,
			},
			width: {
				type: Number,
			},
			height: {
				type: Number,
			},
		}
	],
	img: {
		data: Buffer,
		contentType: String
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Product", productSchema);