const User = require("../models/user.js");
const Product = require("../models/product")
const auth = require("../auth")
const bcrypt = require("bcrypt")

module.exports.registerUser = (body) => {
	let newUser = new User({
		email: body.email,
		password: bcrypt.hashSync(body.password, 10)
	})

	return User.find({email: body.email}).then(result => {
		if(result.length > 0){
			return "Duplicate Email Exist";
		}else{
			return newUser.save().then((user, error) => {
				if(error){
					return "User not registered"; 
				}else{
					return "User registered successfully"; 
				}
			});
		}
	})
}

module.exports.loginUser = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(result === null){
			return false; //user doesn't exist
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)
			if(isPasswordCorrect === true){
				return {access: auth.createAccessToken(result.toObject())}
			}else{
				return false; //passwords didn't match; can't log in
			}
		}
	})
}

module.exports.updateAdmin = (params, body) => {
	let updatedAdmin = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(params.userId, updatedAdmin).then((user, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}

module.exports.removeAdmin = (params, body) => {
	let updatedAdmin = {
		isAdmin: false
	}

	return User.findByIdAndUpdate(params.userId, updatedAdmin).then((user, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}