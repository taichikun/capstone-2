const Product = require('../models/product.js')

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.getProduct = (params) => {
	return Product.findById(params.productId)/*findOne({_id:params.courseId})*/.then(result => {
		return result;
	})
}

module.exports.addProduct = (body) => {

	let newProduct = new Product({
		sku: body.sku,
		productName: body.productName,
		brand: body.brand,
		description: body.description,
		price: body.price,
		qty: body.qty,
		dimensions: [
			{
				weight: body.weight,
				length: body.length,
				width: body.width,
				height: body.height
			}
		]
		
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false; //save unsuccessful
		}else{
			return true; //save successful
		}
	})
}

module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		sku: body.sku,
		productName: body.productName,
		brand: body.brand,
		description: body.description,
		price: body.price,
		qty: body.qty,
		dimensions: [
			{
				weight: body.weight,
				length: body.length,
				width: body.width,
				height: body.height
			}
		]
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}

module.exports.archiveProduct = (params) => {
	let updatedProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}

module.exports.reactivateProduct = (params) => {
	let updatedProduct = {
		isActive: true
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}