const Order = require("../models/order")
const User = require("../models/user.js");
const auth = require("../auth")
const bcrypt = require("bcrypt")

module.exports.getProfile = (userId) => {
	return Order.find({userId: userId}).then(result => {
		result[0].userId = undefined;
		return result;
	})
}

module.exports.getAllOrders = () => {
	return Order.find().then(result => {
		return result;
	})
}

module.exports.checkout = (data) => {
	let newOrder = new Order({
		userId: data.userId,
		totalAmount: data.totalAmount,
		products: data.products
	})

	return newOrder.save().then((order, err) => {
		if (err){
			return false; 
		} else {
			return true; 
		}
	})
}