const express = require('express');
const router = express.Router();
const auth = require('../auth');
const userController = require("../controllers/userController");

// Route for registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromRegister => res.send(resultFromRegister))
})

// Route for logging in
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

// Route to set user as admin
router.put("/:userId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		userController.updateAdmin(req.params, req.body).then(resultFromAdmin => res.send(resultFromAdmin))
	}else{
		res.send({auth: "Not an admin"})
	}
})

// Route to set remove user as admin
router.put("/removeAdmin/:userId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		userController.removeAdmin(req.params, req.body).then(resultFromAdmin => res.send(resultFromAdmin))
	}else{
		res.send({auth: "Not an admin"})
	}
})


module.exports = router;