// set up dependencies
const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController')
const auth =  require("../auth");

// Route to get all active products
router.get("/all", (req, res) => {
	productController.getAllActive().then(resultFromGetAllActive => res.send(resultFromGetAllActive))
})

// Route to get a specific product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromGetSpecific => res.send(resultFromGetSpecific))
})

// Route to create a new product
router.post("/", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.addProduct(req.body).then(resultFromAddProduct => res.send(resultFromAddProduct))
	}else{
		res.send({auth: "Not an admin"})
	}
})

// Route to update a product
router.put("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.updateProduct(req.params, req.body).then(resultFromUpdate => res.send(resultFromUpdate))
	}else{
		res.send({auth: "Not an admin"})
	}
})

// Route to archive a product
router.delete("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.archiveProduct(req.params).then(resultFromArchive => res.send(resultFromArchive))
	}else{
		res.send({auth: "Not an admin"})
	}
})

// Route to reactivate a product
router.delete("/activate/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.reactivateProduct(req.params).then(resultFromArchive => res.send(resultFromArchive))
	}else{
		res.send({auth: "Not an admin"})
	}
})

module.exports = router;