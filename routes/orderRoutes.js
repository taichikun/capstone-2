const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController')
const auth =  require("../auth");

// Route for getting user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	orderController.getProfile(userData.id).then(resultFromGetProfile => res.send(resultFromGetProfile))
})

// Route to get all orders
router.get("/everything", (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		orderController.getAllOrders().then(resultFromGetAllOrders => res.send(resultFromGetAllOrders))
	}else{
		res.send({auth: "Not an admin"})
	}
	
})

// Route for checkout orders
router.post("/checkout", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		orderId: req.body.orderId,
		products: req.body.products,
		totalAmount: req.body.totalAmount
	}
	orderController.checkout(data).then(resultFromCheckout => res.send(resultFromCheckout))
})

module.exports = router;